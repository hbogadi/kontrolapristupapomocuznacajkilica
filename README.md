# KontrolaPristupaPomocuZnacajkiLica

Kontrola pristupa direktorijima na racunalu pomocu facial recognition tehnologije.

Program za skrivanje direktorija. Direktorij se otkriva kada se pomocu kamere prepozna da pred racunalom
sjedi korisnik koji ima vlasnistvo nad tim folderom.

Application hides user selected directories. A directory is revealed when user who locked it is recognized to be 
    sitting in front of the camera.

Big revision:
    Implemented my own face recognition functions using pre-trained model provided by nyoki-mtl
    (https://github.com/nyoki-mtl/keras-facenet).

I haven't trained the model myself because I lack the computing power to do it 
    in a reasonable ammount of time. The model used is Keras friendly model of 
    Facenet implementation (based on this research: https://arxiv.org/abs/1503.03832).

### **This software is just a showcase software and is not intended for actual application.**